from sqlalchemy.orm import backref

from app.database import db


class Client(db.Model):
    __tablename__ = 'client'

    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(40), nullable=False)
    last_name = db.Column(db.String(50), nullable=False)
    phone = db.Column(db.BIGINT, unique=True, nullable=False)
    mail = db.Column(db.String(100), unique=True)
    passport = db.Column(db.String(10), unique=True, nullable=False)
    birthday = db.Column(db.DateTime, nullable=False)

    items = db.relationship('Item', backref=backref('client'))

    def __str__(self):
        return f'Client {self.id}: {self.first_name} {self.last_name} {self.phone} {self.mail} {self.passport}'
