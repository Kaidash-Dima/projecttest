import logging

from app.client import ClientRepository
from app.client.model import Client
from app.exceptions import UniqueFieldConstraintException, RequiredFieldException


class ClientService:

    @classmethod
    def add(cls, client: Client) -> Client:
        logging.info('add_client method was called with: ', client)
        if client.id is None:
            if not cls.client_register_data_check(client):
                raise RequiredFieldException('Require field is empty')

            if ClientRepository.get_by_phone(client.phone):
                raise UniqueFieldConstraintException('Client with this phone is already exist')

            if ClientRepository.get_by_mail(client.mail):
                raise UniqueFieldConstraintException('Client with this mail is already exist')

            if ClientRepository.get_by_passport(client.passport):
                raise UniqueFieldConstraintException('Client with this passport is already exist')

            client = ClientRepository.save_obj(client)

            return client

    @classmethod
    def update(cls, client: Client) -> Client:
        logging.info('upgrade_client method was called with: ', client)
        if client.id is not None and client.id > 0:
            if not cls.client_register_data_check(client):
                raise RequiredFieldException('Require field is empty')

            client_by_id = ClientRepository.get_obj_by_id(client.id)

            client_phone = ClientRepository.get_by_phone(client.phone)
            if client_phone is not None and client_by_id != client_phone:
                raise UniqueFieldConstraintException('Client with this phone is already exist')

            client_mail = ClientRepository.get_by_mail(client.mail)
            if client_mail is not None and client_by_id != client_mail:
                raise UniqueFieldConstraintException('Client with this mail is already exist')

            client_passport = ClientRepository.get_by_passport(client.passport)
            if client_passport is not None and client_by_id != client_passport:
                raise UniqueFieldConstraintException('Client with this passport is already exist')

            client = ClientRepository.save_obj(cls.client_field_replace(client, client_by_id))

            return client

    @classmethod
    def delete(cls, _id) -> bool:
        logging.info('delete_client method was called with: ', _id)
        return ClientRepository.delete_obj_by_id(_id)

    @classmethod
    def get(cls, _id) -> Client:
        logging.info('get_client method was called with: ', _id)
        client = ClientRepository.get_obj_by_id(_id)
        return client

    @classmethod
    def client_register_data_check(cls, client: Client) -> bool:
        return (client.first_name is not None
                and len(client.first_name) <= 40
                and client.last_name is not None
                and len(client.last_name) <= 50
                and client.phone is not None
                and client.mail is not None
                and len(client.mail) <= 100
                and client.mail.find("@", 0, len(client.mail))
                and client.passport is not None
                and len(client.passport) <= 10
                and client.birthday is not None)

    @classmethod
    def client_field_replace(cls, updated_client: Client, client: Client) -> Client:
        client.first_name = updated_client.first_name
        client.last_name = updated_client.last_name
        client.phone = updated_client.phone
        client.mail = updated_client.mail
        client.passport = updated_client.passport
        return client
