from marshmallow.fields import Pluck

from app.client.model import Client
from app.database import marshmallow, session
from app.item.schema import ItemSchema


class ClientSchema(marshmallow.ModelSchema):
    class Meta:
        model = Client
        load_instance = True
        include_fk = False
        sqla_session = session

        items = Pluck(ItemSchema(only=["id"]), "id", many=True)
