from app.basic_repository import BasicRepository

from app.client import ClientModel


class ClientRepository(BasicRepository):
    model = ClientModel

    @classmethod
    def get_by_phone(cls, phone):
        return super().get_obj_by_field('phone', phone)

    @classmethod
    def get_by_mail(cls, mail):
        return super().get_obj_by_field('mail', mail)

    @classmethod
    def get_by_passport(cls, passport):
        return super().get_obj_by_field('passport', passport)

