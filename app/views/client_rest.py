import logging
from flask import Blueprint, request, abort
from marshmallow import ValidationError

from app.client.schema import ClientSchema
from app.client.service import ClientService
from app.exceptions import RequiredFieldException, UniqueFieldConstraintException

client_rest = Blueprint('client_rest', __name__)


@client_rest.route('', methods=['POST'])
def create_client():
    logging.info('create_client method was called with: ', request.json)

    try:
        client_schema = ClientSchema()
        client = client_schema.load(request.json)
        client.id = None
        client_answer = ClientService.add(client)
        if client_answer is not None:
            return ClientSchema().dump(client_answer), 201
    except RequiredFieldException as rfe:
        abort(400, rfe)
    except UniqueFieldConstraintException as ufce:
        abort(400, ufce)
    except ValidationError as ve:
        abort(400, ve)
    else:
        abort(500)


@client_rest.route('', methods=["PUT"])
def update_client():
    logging.info('update_client method was called with: ', request.json)

    if not request.json or request.json["id"] is None or request.json["id"] <= 0:
        abort(400)

    try:
        client = ClientSchema().load(request.json)
        client_answer = ClientService.update(client)
        if client_answer is not None:
            return ClientSchema().dump(client_answer), 200
    except RequiredFieldException as rfe:
        abort(400, rfe)
    except UniqueFieldConstraintException as ufce:
        abort(400, ufce)
    except ValidationError as ve:
        abort(400, ve)
    else:
        abort(500)


@client_rest.route('/<int:id>', methods=['DELETE'])
def delete_client(id):
    logging.info('delete_client method was called with: ', request.json)

    try:
        if ClientService.delete(id):
            return "", 200
        else:
            abort(404)
    except ValidationError as ve:
        abort(400, ve)
    else:
        abort(500)


@client_rest.route('/<int:id>', methods=['GET'])
def get_client(id):
    logging.info('get_client method was called with: ', request.json)

    try:
        client_answer = ClientService.get(id)
        if client_answer:
            return ClientSchema().dump(client_answer), 200
        else:
            abort(404)
    except ValidationError as ve:
        abort(400, ve)
    else:
        abort(500)
