import logging
from flask import Blueprint, request, abort
from marshmallow import ValidationError

from app.exceptions import RequiredFieldException, UniqueFieldConstraintException
from app.item.schema import ItemSchema
from app.item.service import ItemService

item_rest = Blueprint('item_rest', __name__)


@item_rest.route('', methods=['POST'])
def create_item():
    logging.info('create_item method was called with: ', request.json)

    try:
        item_schema = ItemSchema()
        item = item_schema.load(request.json)
        item.id = None
        item_answer = ItemService.add(item)
        if item_answer is not None:
            return item_schema.dump(item_answer), 201
    except RequiredFieldException as rfe:
        abort(400, rfe)
    except UniqueFieldConstraintException as ufce:
        abort(400, ufce)
    except ValidationError as ve:
        abort(400, ve)
    else:
        abort(500)


@item_rest.route('', methods=['PUT'])
def update_item():
    logging.info('update_item method was called with: ', request.json)

    if not request.json or request.json["id"] is None or request.json["id"] <= 0:
        abort(400)

    try:
        item = ItemSchema().load(request.json)
        item_answer = ItemService.update(item)
        if item_answer is not None:
            return ItemSchema().dump(item_answer), 200
    except RequiredFieldException as rfe:
        abort(400, rfe)
    except UniqueFieldConstraintException as ufce:
        abort(400, ufce)
    except ValidationError as ve:
        abort(400, ve)
    else:
        abort(500)


@item_rest.route('/<int:id>', methods=['DELETE'])
def delete_item(id):
    logging.info('delete_item method was called with: ', request.json)

    try:
        if ItemService.delete(id):
            return "", 200
        else:
            abort(404)
    except ValidationError as ve:
        abort(400, ve)
    else:
        abort(500)


@item_rest.route('/<int:id>', methods=['GET'])
def get_item(id):
    logging.info('get_item method was called with: ', request.json)

    try:
        item_answer = ItemService.get(id)
        if item_answer:
            return ItemSchema().dump(item_answer), 200
        else:
            abort(404)
    except ValidationError as ve:
        abort(400, ve)
    else:
        abort(500)
