from abc import ABC, abstractproperty

import logging

from sqlalchemy.exc import SQLAlchemyError

from app.database import session


class BasicRepository(ABC):
    model = abstractproperty()

    @classmethod
    def save_obj(cls, obj) -> model:
        if obj is not None:
            try:
                session.add(obj)
                session.commit()
                session.refresh(obj)
                return obj
            except SQLAlchemyError as ex:
                logging.error(
                    'An error occurred while'
                    ' save_obj method was called: %s',
                    ex)
                session.rollback()

    @classmethod
    def save_obj_by_attrs(cls, **attrs: object) -> model:
        return cls.save_obj(cls.model(**attrs))

    @classmethod
    def save_all_objects(cls, objects: []) -> list:
        if objects is not None and len(objects) > 0:
            try:
                session.add_all(objects)
                session.commit()
                for _obj in objects:
                    session.refresh(_obj)
                return objects
            except SQLAlchemyError as ex:
                logging.error(
                    'An error occurred while'
                    ' save_all_obj method was called: %s',
                    ex)
        return []

    @classmethod
    def delete_obj_by_id(cls, _id) -> bool:
        if _id is not None:
            try:
                obj = session.query(cls.model).get(_id)
                session.delete(obj)
                session.commit()
                return True
            except SQLAlchemyError as ex:
                logging.error(
                    'An error occurred while'
                    ' delete_obj method was called: %s',
                    ex)
                session.rollback()
        return False

    @classmethod
    def delete_obj(cls, obj) -> bool:
        if obj is not None:
            try:
                session.delete(obj)
                session.commit()
                return True
            except SQLAlchemyError as ex:
                logging.error(
                    'An error occurred while'
                    ' delete_obj method was called: %s',
                    ex)
                session.rollback()
        return False

    @classmethod
    def delete_all_objects(cls, objects: []) -> bool:
        if objects is not None and len(objects) > 0:
            try:
                for _obj in objects:
                    session.delete(_obj)
                session.commit()
                return True
            except SQLAlchemyError as ex:
                logging.error(
                    'An error occurred while'
                    ' delete_all_objects method was called: %s',
                    ex)
                session.rollback()
        return False

    @classmethod
    def get_obj_by_field(cls, field: str, value) -> model:
        if hasattr(cls.model, field):
            try:
                return session.query(cls.model).filter(
                    getattr(cls.model, field) == value).first()
            except SQLAlchemyError as ex:
                logging.error(
                    'An error occurred while'
                    ' get_obj_by_field method was called: %s',
                    ex)

    @classmethod
    def get_all_obj_by_field(cls, field: str, value) -> list:
        if hasattr(cls.model, field):
            try:
                return session.query(cls.model).filter(
                    getattr(cls.model, field) == value).all()
            except SQLAlchemyError as ex:
                logging.error(
                    'An error occurred while'
                    ' get_all_obj_by_field method was called: %s',
                    ex)
        return []

    @classmethod
    def get_obj_by_id(cls, _id: int) -> model:
        if _id is not None:
            try:
                return session.query(cls.model).get(_id)
            except SQLAlchemyError as ex:
                logging.error(
                    'An error occurred while'
                    ' get_by_id method was called: %s',
                    ex)
