class UniqueFieldConstraintException(Exception):
    pass


class RequiredFieldException(Exception):
    pass
