from flask import Flask

from app.views.item_rest import item_rest
from app.views.client_rest import client_rest
from app import item
from app import client

from app.database import db, migrate, marshmallow


def create_app(config_object):
    app = Flask(__name__)
    app.config.from_object(config_object)

    db.init_app(app)
    app.session = database.session

    migrate.init_app(app, db)

    marshmallow.init_app(app)

    app.register_blueprint(client_rest, url_prefix='/api/clients')
    app.register_blueprint(item_rest, url_prefix='/api/items')

    return app
