import logging

from app.exceptions import RequiredFieldException
from app.item import ItemRepository
from app.item.model import Item


class ItemService:

    @classmethod
    def add(cls, item: Item) -> Item:
        logging.info('add_item method was called with: ', item)
        if item.id is None:
            if not cls.item_register_data_check(item):
                raise RequiredFieldException('Require field is empty')
            return ItemRepository.save_obj(item)

    @classmethod
    def update(cls, item: Item) -> Item:
        logging.info('upgrade_item method was called with: ', item)
        if item.id is not None and item.id > 0:
            if not cls.item_register_data_check(item):
                raise RequiredFieldException('Require field is empty')
            return ItemRepository.save_obj(
                cls.item_field_replace(item, ItemRepository.get_obj_by_id(item.id)))

    @classmethod
    def delete(cls, _id) -> bool:
        logging.info('delete_item method was called with: ', _id)
        return ItemRepository.delete_obj_by_id(_id)

    @classmethod
    def get(cls, _id) -> Item:
        logging.info('get_item method was called with: ', _id)
        return ItemRepository.get_obj_by_id(_id)

    @classmethod
    def item_register_data_check(cls, item: Item) -> bool:
        return (item.weight is not None
                and item.description is not None
                and len(item.description) <= 256
                and item.cost is not None
                and item.date is not None
                and item.client_id is not None)

    @classmethod
    def item_field_replace(cls, updated_item: Item, item: Item) -> Item:
        item.description = updated_item.description
        item.cost = updated_item.cost
        item.client_id = updated_item.client_id
        return item
