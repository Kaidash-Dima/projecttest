from app.basic_repository import BasicRepository
from app.item import ItemModel


class ItemRepository(BasicRepository):
    model = ItemModel

    @classmethod
    def get_all_obj_by_ids(cls, ids: []) -> list:
        items = []
        for j in range(len(ids)):
            item = cls.get_obj_by_id(ids[j])
            if item is not None:
                items.append(item)

        return items
