from app.item.model import Item
from app.database import marshmallow, session


class ItemSchema(marshmallow.ModelSchema):
    class Meta:
        model = Item
        include_fk = True
        sqla_session = session
