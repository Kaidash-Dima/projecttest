from app.database import db


class Item(db.Model):
    __tablename__ = 'items'

    id = db.Column(db.Integer, primary_key=True)
    weight = db.Column(db.Float, nullable=False)
    volume = db.Column(db.Float, nullable=False)
    description = db.Column(db.String(256), nullable=False)
    cost = db.Column(db.Float, nullable=False)
    date = db.Column(db.DateTime, nullable=False)

    client_id = db.Column(db.Integer, db.ForeignKey('client.id'))

    def __str__(self):
        return f'Item {self.id}: {self.weight} {self.volume} {self.description} {self.cost} {self.date}'
