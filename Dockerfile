FROM python:3.6
ARG DATABASE_URL
ARG APP_SETTINGS

ENV PYTHONUNBUFFERED=1
ENV DATABASE_URL="$DATABASE_URL"
ENV APP_SETTINGS="$APP_SETTINGS"
ENV FLASK_APP="manage.py"

COPY . /usr/src/app
WORKDIR /usr/src/app

RUN pip install -r requirements/production.txt
RUN flask db upgrade

CMD gunicorn -b 0.0.0.0:5000 manage:app