import json
from unittest import TestCase, mock

from requests import Response

from app.client import ClientSchema
from app.client.model import Client
from app.database import session
from app.item import ItemSchema
from manage import app
from test.client import client_dict_generator
from test.item import item_dict_generator


class ClientRestTest(TestCase):
    client_generator = client_dict_generator()
    item_generator = item_dict_generator()

    def setUp(self):
        self.context = app.test_request_context()
        self.context.push()

        self.client_schema = ClientSchema()
        self.item_schema = ItemSchema()

    def tearDown(self) -> None:
        session.close()
        self.context.pop()

    def test_create_client(self):
        client_dict = next(self.client_generator)
        client_dict["items"] = []

        client_dict2 = client_dict
        client_dict2["id"] = 1

        client_schema = ClientSchema()

        client1 = Client(**client_dict)
        client2 = Client(**client_dict2)

        with mock.patch('app.client.service.ClientService.add', return_value=client2):
            with app.test_client() as client:
                response: Response = client.post(
                    "/api/clients",
                    content_type='application/json',
                    data=json.dumps(client_schema.dump(client1))
                )
            self.assertTrue(response.status_code == 201)
            self.assertIsNotNone(response.json)
            self.assertEqual(client2.id, response.json["id"])

    def test_update_client(self):
        client_dict = next(self.client_generator)
        client_dict["id"] = 1
        client_dict["items"] = []

        client_dict2 = client_dict
        client_dict2["phone"] = 380567843

        client_schema = ClientSchema()
        client1 = Client(**client_dict)
        client2 = Client(**client_dict2)

        with mock.patch('app.client.service.ClientService.update', return_value=client2):
            with app.test_client() as client:
                response: Response = client.put(
                    "/api/clients",
                    content_type='application/json',
                    data=json.dumps(client_schema.dump(client1)))
            self.assertTrue(response.status_code == 200)
            self.assertIsNotNone(response.json)
            self.assertEqual(client2.id, response.json['id'])

    def test_delete_client(self):
        with mock.patch('app.client.service.ClientService.delete', return_value=True):
            with app.test_client() as client:
                response: Response = client.delete("/api/clients/1")
            self.assertTrue(response.status_code == 200)

    def test_get_client(self):
        client_dict = next(self.client_generator)
        client_dict["id"] = 1
        client_dict["items"] = []

        client1 = Client(**client_dict)

        with mock.patch('app.client.service.ClientService.get', return_value=client1):
            with app.test_client() as client:
                response: Response = client.get("/api/clients/1")
            self.assertTrue(response.status_code == 200)
            self.assertIsNotNone(response.json)
            self.assertEqual(client1.id, response.json['id'])
