import json
from unittest import TestCase, mock

from requests import Response

from app.client import ClientSchema
from app.database import session
from app.item import ItemSchema
from app.item.model import Item
from manage import app
from test.client import client_dict_generator
from test.item import item_dict_generator


class ItemRestTest(TestCase):
    client_generator = client_dict_generator()
    item_generator = item_dict_generator()

    def setUp(self):
        self.ctx = app.test_request_context()
        self.ctx.push()

        self.client_schema = ClientSchema()
        self.item_schema = ItemSchema()

    def tearDown(self) -> None:
        session.close()
        self.ctx.pop()

    def test_create_items(self):
        item_dict = next(self.item_generator)
        item_dict["client_id"] = 1

        item_dict2 = item_dict
        item_dict2["id"] = 1

        item_schema = ItemSchema()

        item1 = Item(**item_dict)
        item2 = Item(**item_dict2)

        with mock.patch('app.item.service.ItemService.add', return_value=item2):
            with app.test_client() as client:
                response: Response = client.post(
                    "/api/items",
                    content_type='application/json',
                    data=json.dumps(item_schema.dump(item1))
                )
            self.assertTrue(response.status_code == 201)
            self.assertIsNotNone(response.json)
            self.assertEqual(item2.id, response.json["id"])

    def test_update_item(self):
        item_dict = next(self.item_generator)
        item_dict["id"] = 1
        item_dict["client_id"] = 1

        item_dict2 = item_dict
        item_dict2["description"] = "oh yes"

        item_schema = ItemSchema()
        item1 = Item(**item_dict)
        item2 = Item(**item_dict2)

        with mock.patch('app.item.service.ItemService.update', return_value=item2):
            with app.test_client() as client:
                response: Response = client.put(
                    "/api/items",
                    content_type='application/json',
                    data=json.dumps(item_schema.dump(item1))
                )
            self.assertTrue(response.status_code == 200)
            self.assertIsNotNone(response.json)
            self.assertEqual(item2.id, response.json["id"])

    def test_delete_item(self):
        with mock.patch('app.item.service.ItemService.delete', return_value=True):
            with app.test_client() as client:
                response: Response = client.delete("/api/items/1")
            self.assertTrue(response.status_code == 200)

    def test_get_item(self):
        item_dict = next(self.item_generator)
        item_dict["id"] = 1
        item_dict["client_id"] = 1

        item1 = Item(**item_dict)

        with mock.patch('app.item.service.ItemService.get', return_value=item1):
            with app.test_client() as client:
                response: Response = client.get("/api/items/1")
            self.assertTrue(response.status_code == 200)
            self.assertIsNotNone(response.json)
            self.assertEqual(item1.id, response.json["id"])
