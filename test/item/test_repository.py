import unittest

from app.database import db, session
from manage import app
from app.item import ItemModel, ItemRepository
from app.client import ClientModel
from test.client import client_dict_generator
from test.item import item_dict_generator


class ClientRepositoryTest(unittest.TestCase):
    item_generator = item_dict_generator()
    client_generator = client_dict_generator()

    def setUp(self):
        app.test_request_context().push()
        db.drop_all()
        db.create_all()

        client_dict = next(self.client_generator)
        session.add(ClientModel(**client_dict))
        self.client = session.query(ClientModel).first()

        item_dict = next(self.item_generator)
        session.add(ItemModel(**item_dict))
        self.item = session.query(ItemModel).first()

        session.commit()

        self.assertEqual(session.query(ClientModel).count(), 1)
        self.assertEqual(session.query(ItemModel).count(), 1)

    def tearDown(self) -> None:
        session.close()

    def test_get_all_obj_by_ids(self):
        for j in range(3):
            item_dict = next(self.item_generator)
            item_dict['client'] = self.client
            session.add(ItemModel(**item_dict))
        session.commit()

        self.assertEqual(session.query(ClientModel).count(), 1)
        self.assertEqual(session.query(ItemModel).count(), 4)

        len_item = []
        for j in range(4):
            test_item = session.query(ItemModel).all()[j]
            len_item.append(test_item.id)

        self.assertTrue(len(len_item) == 4)

        test_item = ItemRepository.get_all_obj_by_ids(len_item)

        self.assertTrue(len(test_item) == 4)

    def test_save_obj(self):
        item_dict = next(self.item_generator)
        item_dict['client'] = self.client
        item = ItemRepository.save_obj(ItemModel(**item_dict))

        self.assertTrue(item.id > 0)
        self.assertEqual(session.query(ClientModel).count(), 1)
        self.assertEqual(session.query(ItemModel).count(), 2)

    def test_save_obj_by_attrs(self):
        item_dict = next(self.item_generator)
        item_dict['client'] = self.client
        item = ItemRepository.save_obj_by_attrs(**item_dict)

        self.assertTrue(item.id > 0)
        self.assertEqual(session.query(ClientModel).count(), 1)
        self.assertEqual(session.query(ItemModel).count(), 2)

    def test_save_all_objects(self):
        items = []
        for j in range(10):
            item_dict = next(self.item_generator)
            item_dict['client'] = self.client
            items.append(ItemModel(**item_dict))

        test_item = ItemRepository.save_all_objects(items)

        self.assertIsNotNone(test_item)
        self.assertTrue(len(test_item) == 10)
        self.assertEqual(session.query(ClientModel).count(), 1)
        self.assertEqual(session.query(ItemModel).count(), 11)

    def test_delete_obj_by_id(self):
        item = session.query(ItemModel).first()

        ItemRepository.delete_obj_by_id(item.id)

        self.assertEqual(session.query(ClientModel).count(), 1)
        self.assertEqual(session.query(ItemModel).count(), 0)

    def test_delete_obj(self):
        item = session.query(ItemModel).first()

        ItemRepository.delete_obj(item)

        self.assertEqual(session.query(ClientModel).count(), 1)
        self.assertEqual(session.query(ItemModel).count(), 0)

    def test_delete_all_objects(self):
        for j in range(10):
            item_dict = next(self.item_generator)
            item_dict['client'] = self.client
            session.add(ItemModel(**item_dict))
        session.commit()

        self.assertEqual(session.query(ClientModel).count(), 1)
        self.assertEqual(session.query(ItemModel).count(), 11)

        test_item = session.query(ItemModel).all()

        ItemRepository.delete_all_objects(test_item)

        self.assertEqual(session.query(ClientModel).count(), 1)
        self.assertEqual(session.query(ItemModel).count(), 0)

    def test_get_obj_by_field(self):
        item = session.query(ItemModel).first()

        self.assertIsNotNone(item)

        test_item = ItemRepository.get_obj_by_field('description', item.description)

        self.assertTrue(test_item)
        self.assertTrue(item.id == test_item.id)

        test_item = ItemRepository.get_obj_by_field('description', 'bad bag')
        self.assertIsNone(test_item)

    def test_get_all_obj_by_field(self):
        test = session.query(ItemModel).first()

        for j in range(10):
            item_dict = next(self.item_generator)
            item_dict['client'] = self.client
            item_dict['cost'] = 20
            session.add(ItemModel(**item_dict))
        session.commit()

        test_item = ItemRepository.get_all_obj_by_field('cost', 20)

        self.assertEqual(len(test_item), 10)
        self.assertEqual(session.query(ClientModel).count(), 1)
        self.assertEqual(session.query(ItemModel).count(), 11)

        test_item = ItemRepository.get_all_obj_by_field('cost', test.cost)
        self.assertEqual(len(test_item), 1)

        test_item = ItemRepository.get_all_obj_by_field('cost', 2)
        self.assertEqual(len(test_item), 0)

    def test_get_obj_by_id(self):
        item = session.query(ItemModel).first()

        test_item = ItemRepository.get_obj_by_id(item.id)

        self.assertIsNotNone(test_item)
        self.assertTrue(item.id == test_item.id)

        test_item = ItemRepository.get_obj_by_id(8500)
        self.assertIsNone(test_item)
