import unittest

from flask.testing import FlaskClient

from app import db
from app.database import session
from app.exceptions import RequiredFieldException
from app.item.model import Item
from app.item.service import ItemService
from manage import app
from test.client import client_dict_generator
from test.item import item_dict_generator
from app.client.model import Client


class TestItemService(unittest.TestCase):
    client_generator = client_dict_generator()
    item_generator = item_dict_generator()

    def setUp(self):
        app.test_request_context().push()

        self.client: FlaskClient = app.test_client()

        db.drop_all()
        db.create_all()

        client_dict = next(self.client_generator)
        session.add(Client(**client_dict))
        session.commit()

        self.client = session.query(Client).first()
        item_dict = next(self.item_generator)
        item_dict["client"] = self.client
        session.add(Item(**item_dict))
        session.commit()

        self.item = session.query(Item).first()

        self.assertEqual(session.query(Client).count(), 1)
        self.assertEqual(session.query(Item).count(), 1)

    def tearDown(self) -> None:
        session.close()

    def test_add(self):
        item_dict = next(self.item_generator)
        item_dict['id'] = None
        item_dict['client_id'] = self.client.id

        test_item = ItemService.add(Item(**item_dict))

        item = session.query(Item).all()[1]

        self.assertIsNotNone(test_item)
        self.assertEqual(session.query(Item).count(), 2)
        self.assertTrue(test_item.id == item.id)

    def test_add_with_non_require(self):
        item_dict = next(self.item_generator)
        item_dict['id'] = None
        item_dict['weight'] = None
        item_dict['client_id'] = self.client.id

        with self.assertRaises(RequiredFieldException):
            ItemService.add(Item(**item_dict))

    def test_update(self):
        item_dict = next(self.item_generator)
        item_dict['id'] = None
        item_dict['client_id'] = self.client.id

        session.add(Item(**item_dict))
        session.commit()

        self.assertEqual(session.query(Item).count(), 2)

        item = session.query(Item).all()[1]
        item_dict['id'] = item.id
        item_dict['cost'] = 50.56

        item_update = ItemService.update(Item(**item_dict))

        self.assertEqual(item_update.cost, 50.56)

    def test_update_with_non_require(self):
        item_dict = next(self.item_generator)
        item_dict['id'] = None
        item_dict['weight'] = None
        item_dict['client_id'] = self.client.id

        with self.assertRaises(RequiredFieldException):
            ItemService.add(Item(**item_dict))

    def test_delete(self):
        test_item = session.query(Item).first()

        ItemService.delete(test_item.id)

        self.assertEqual(session.query(Item).count(), 0)
        self.assertEqual(session.query(Client).count(), 1)

    def test_get(self):
        item = session.query(Item).first()

        test_item = ItemService.get(item.id)

        self.assertTrue(item.id == test_item.id)
