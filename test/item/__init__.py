from datetime import datetime


def item_dict_generator():
    i = 1

    while True:
        item = dict(weight=10 + i,
                    volume=20 + i,
                    description=f'My bag{i}',
                    cost=2 + i,
                    date=datetime.now())

        yield {**item}
        i += 1
