import unittest

from app.database import session, db
from manage import app
from app.client import ClientRepository, ClientModel
from app.item import ItemModel
from test.client import client_dict_generator
from test.item import item_dict_generator


class ClientRepositoryTest(unittest.TestCase):
    client_generator = client_dict_generator()
    item_generator = item_dict_generator()

    def setUp(self):
        app.test_request_context().push()
        db.drop_all()
        db.create_all()

        client_dict = next(self.client_generator)
        session.add(ClientModel(**client_dict))
        self.client = session.query(ClientModel).first()

        item_dict = next(self.item_generator)
        session.add(ItemModel(**item_dict))
        self.item = session.query(ItemModel).first()

        session.commit()

        self.assertEqual(session.query(ClientModel).count(), 1)
        self.assertEqual(session.query(ItemModel).count(), 1)

    def tearDown(self) -> None:
        session.close()

    def test_get_by_phone(self):
        for j in range(2):
            client_dict = next(self.client_generator)
            session.add(ClientModel(**client_dict))
        session.commit()

        self.assertEqual(session.query(ClientModel).count(), 3)
        self.assertEqual(session.query(ItemModel).count(), 1)
        test_client = session.query(ClientModel).all()[2]

        client = ClientRepository.get_by_phone(test_client.phone)
        self.assertIsNotNone(client)
        self.assertTrue(client.id == test_client.id)
        self.assertTrue(client.phone == test_client.phone)

        client = ClientRepository.get_by_phone('1234567890')
        self.assertIsNone(client)

    def test_get_by_mail(self):
        for j in range(2):
            client_dict = next(self.client_generator)
            session.add(ClientModel(**client_dict))
        session.commit()

        self.assertEqual(session.query(ClientModel).count(), 3)
        self.assertEqual(session.query(ItemModel).count(), 1)
        test_client = session.query(ClientModel).all()[2]

        client = ClientRepository.get_by_mail(test_client.mail)

        self.assertIsNotNone(client)
        self.assertTrue(client.id == test_client.id)
        self.assertTrue(client.mail == test_client.mail)

        client = ClientRepository.get_by_mail('Mail@gmail.com')
        self.assertIsNone(client)

    def test_get_by_passport(self):
        for j in range(2):
            client_dict = next(self.client_generator)
            session.add(ClientModel(**client_dict))
        session.commit()

        self.assertEqual(session.query(ClientModel).count(), 3)
        self.assertEqual(session.query(ItemModel).count(), 1)
        test_client = session.query(ClientModel).all()[2]

        client = ClientRepository.get_by_passport(test_client.passport)

        self.assertIsNotNone(client)
        self.assertTrue(client.id == test_client.id)
        self.assertTrue(client.passport == test_client.passport)

        client = ClientRepository.get_by_passport('899203')
        self.assertIsNone(client)

    def test_save_obj(self):
        client_dict = next(self.client_generator)
        client = ClientRepository.save_obj(ClientModel(**client_dict))

        self.assertTrue(client.id > 0)
        self.assertEqual(session.query(ClientModel).count(), 2)
        self.assertEqual(session.query(ItemModel).count(), 1)

    def test_save_all_objects(self):
        client = []
        for j in range(10):
            client_dict = next(self.client_generator)
            client.append(ClientModel(**client_dict))
        test_client = ClientRepository.save_all_objects(client)

        self.assertTrue(len(test_client) == 10)
        self.assertEqual(session.query(ClientModel).count(), 11)
        self.assertEqual(session.query(ItemModel).count(), 1)

    def test_save_obj_by_attrs(self):
        client_dict = next(self.client_generator)
        client = ClientRepository.save_obj_by_attrs(**client_dict)

        self.assertTrue(client.id > 0)
        self.assertEqual(session.query(ClientModel).count(), 2)
        self.assertEqual(session.query(ItemModel).count(), 1)

    def test_delete_obj_by_id(self):
        client = session.query(ClientModel).first()

        ClientRepository.delete_obj_by_id(client.id)

        self.assertEqual(session.query(ClientModel).count(), 0)
        self.assertEqual(session.query(ItemModel).count(), 1)

    def test_delete_obj(self):
        client = session.query(ClientModel).first()

        ClientRepository.delete_obj(client)

        self.assertEqual(session.query(ClientModel).count(), 0)
        self.assertEqual(session.query(ItemModel).count(), 1)

    def test_delete_all_objects(self):
        for j in range(10):
            client_dict = next(self.client_generator)
            session.add(ClientModel(**client_dict))
        session.commit()

        self.assertEqual(session.query(ClientModel).count(), 11)
        self.assertEqual(session.query(ItemModel).count(), 1)

        client = session.query(ClientModel).all()

        ClientRepository.delete_all_objects(client)

        self.assertEqual(session.query(ClientModel).count(), 0)
        self.assertEqual(session.query(ItemModel).count(), 1)

    def test_get_obj_by_field(self):
        client = session.query(ClientModel).first()

        self.assertIsNotNone(client)

        test_client = ClientRepository.get_obj_by_field('phone', client.phone)

        self.assertIsNotNone(test_client)
        self.assertTrue(test_client.phone == client.phone)

        test_client = ClientRepository.get_obj_by_field('random', '335236')
        self.assertIsNone(test_client)

    def test_get_all_obj_by_field(self):
        for j in range(5):
            client_dict = next(self.client_generator)
            client_dict['first_name'] = 'name1'
            session.add(ClientModel(**client_dict))
        session.commit()

        self.assertEqual(session.query(ClientModel).count(), 6)

        test_client = ClientRepository.get_all_obj_by_field('first_name', 'name1')

        self.assertIsNotNone(test_client)

        self.assertEqual(len(test_client), 5)

    def test_get_obj_by_id(self):
        test_client = session.query(ClientModel).first()

        client = ClientRepository.get_obj_by_id(test_client.id)

        self.assertIsNotNone(client)
        self.assertTrue(client.id == test_client.id)

        client = ClientRepository.get_obj_by_id(8500)
        self.assertIsNone(client)
