import unittest

from flask.testing import FlaskClient

from app import db
from app.client.service import ClientService
from app.database import session
from app.exceptions import UniqueFieldConstraintException, RequiredFieldException
from manage import app
from test.client import client_dict_generator
from app.client.model import Client
from app.item.model import Item
from test.item import item_dict_generator


class TestClientService(unittest.TestCase):
    client_generator = client_dict_generator()
    item_generator = item_dict_generator()

    def setUp(self) -> None:
        app.test_request_context().push()

        self.client: FlaskClient = app.test_client()

        db.drop_all()
        db.create_all()
        self.assertEqual(session.query(Client).count(), 0)
        self.assertEqual(session.query(Item).count(), 0)

        client_dict = next(self.client_generator)
        session.add(Client(**client_dict))
        session.commit()

        self.client = session.query(Client).first()
        item_dict = next(self.item_generator)
        item_dict["client"] = self.client
        session.add(Item(**item_dict))
        session.commit()

        self.item = session.query(Item).first()

        self.assertEqual(session.query(Client).count(), 1)
        self.assertEqual(session.query(Item).count(), 1)

    def tearDown(self) -> None:
        session.close()

    def test_add(self):
        client_dict = next(self.client_generator)
        client_dict['id'] = None
        client_dict['items'] = []
        client = ClientService.add(Client(**client_dict))

        self.assertIsNotNone(client)
        self.assertEqual(session.query(Client).count(), 2)
        self.assertEqual(session.query(Item).count(), 1)

    def test_add_with_non_require(self):
        client_dict = next(self.client_generator)
        client_dict['id'] = None
        client_dict['first_name'] = None
        client_dict['items'] = []

        with self.assertRaises(RequiredFieldException):
            ClientService.add(Client(**client_dict))

    def test_add_with_non_unique_phone(self):
        client_first = session.query(Client).first()

        client_dict = next(self.client_generator)
        client_dict['id'] = None
        client_dict['items'] = []
        client_dict['phone'] = client_first.phone

        with self.assertRaises(UniqueFieldConstraintException):
            ClientService.add(Client(**client_dict))

    def test_add_with_non_unique_mail(self):
        client_first = session.query(Client).first()

        client_dict = next(self.client_generator)
        client_dict['id'] = None
        client_dict['items'] = []
        client_dict['mail'] = client_first.mail

        with self.assertRaises(UniqueFieldConstraintException):
            ClientService.add(Client(**client_dict))

    def test_add_with_non_unique_passport(self):
        client_first = session.query(Client).first()

        client_dict = next(self.client_generator)
        client_dict['id'] = None
        client_dict['items'] = []
        client_dict['passport'] = client_first.passport

        with self.assertRaises(UniqueFieldConstraintException):
            ClientService.add(Client(**client_dict))

    def test_update(self):
        client_dict = next(self.client_generator)
        session.add(Client(**client_dict))
        session.commit()

        self.assertEqual(session.query(Client).count(), 2)

        client = session.query(Client).filter(Client.passport == client_dict['passport']).first()
        client_dict['id'] = client.id
        client_dict['passport'] = 'qwerty'
        client_dict['items'] = []

        client_update = ClientService.update(Client(**client_dict))

        self.assertEqual(client_update.passport, 'qwerty')

    def test_update_with_non_require(self):
        client_dict = next(self.client_generator)
        client_dict['id'] = None
        client_dict['first_name'] = None
        client_dict['items'] = []

        with self.assertRaises(RequiredFieldException):
            ClientService.add(Client(**client_dict))

    def test_update_with_non_unique_phone(self):
        client_dict = next(self.client_generator)
        session.add(Client(**client_dict))
        session.commit()

        client_first = session.query(Client).all()[1]
        client_second = next(self.client_generator)

        client_second['id'] = self.client.id
        client_second['phone'] = client_first.phone
        client_second['items'] = []

        with self.assertRaises(UniqueFieldConstraintException):
            ClientService.update(Client(**client_second))

    def test_update_with_non_unique_mail(self):
        client_dict = next(self.client_generator)
        session.add(Client(**client_dict))
        session.commit()

        client_first = session.query(Client).all()[1]
        client_second = next(self.client_generator)

        client_second['id'] = self.client.id
        client_second['mail'] = client_first.mail
        client_second['items'] = []

        with self.assertRaises(UniqueFieldConstraintException):
            ClientService.update(Client(**client_second))

    def test_update_with_non_unique_passport(self):
        client_dict = next(self.client_generator)
        session.add(Client(**client_dict))
        session.commit()

        client_first = session.query(Client).all()[1]
        client_second = next(self.client_generator)

        client_second['id'] = self.client.id
        client_second['passport'] = client_first.passport
        client_second['items'] = []

        with self.assertRaises(UniqueFieldConstraintException):
            ClientService.update(Client(**client_second))

    def test_delete(self):
        client_test = session.query(Client).first()

        ClientService.delete(client_test.id)

        self.assertEqual(session.query(Client).count(), 0)
        self.assertEqual(session.query(Item).count(), 1)

    def test_get(self):
        client = session.query(Client).first()

        test_client = ClientService.get(client.id)

        self.assertTrue(client.id == test_client.id)
