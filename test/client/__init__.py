from datetime import datetime


def client_dict_generator():
    i = 1

    while True:
        client = dict(first_name=f'name{i}',
                      last_name=f'last_name{i}',
                      phone=1999631 + (i * i),
                      mail=f'mail@mail{i}.com',
                      passport=f'passport{i}',
                      birthday=datetime.now())

        yield {**client}
        i += 1
