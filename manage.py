import os
from app import create_app
from werkzeug.utils import import_string

config = import_string(os.environ['APP_SETTINGS'])()
app = create_app(config)

if __name__ == '__main__':
    app.run()
