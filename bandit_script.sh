##!/bin/bash
bandit_level=$(jq -r '.metrics._totals."SEVERITY.HIGH"' bandit/bandit.json)

if [ "$bandit_level" -gt 0 ];
then
  BANDIT_LEVEL=HIGH
else
  bandit_level=$(jq -r '.metrics._totals."SEVERITY.MEDIUM"' bandit/bandit.json)
  if [ "$bandit_level" -gt 0 ];
  then
    BANDIT_LEVEL=MEDIUM
  else
    bandit_level=$(jq -r '.metrics._totals."SEVERITY.LOW"' bandit/bandit.json)
    if [ "$bandit_level" -gt 0 ];
    then
      BANDIT_LEVEL=LOW
    else
      BANDIT_LEVEL=SECURE
    fi
  fi
fi

echo $BANDIT_LEVEL