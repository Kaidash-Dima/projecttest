Projecttest
This is a trial version of my project
Installation
The following operations are required to start the project
1. Install Python 3.6
download from https://www.python.org/downloads/ - seek for the latest release.
2. Add the following paths to the environment variables
More info about flask variables is here(https://the-bosha.ru/2016/06/03/python-flask-freimvork-pravilnaia-struktura-prilozheniia/)
Also add this properties to the environment variables:
FLASK_APP:
manage.py
APP_SETTINGS:
config.DevelopmentConfig or config.ProductionConfig
For development it's proposed to make your own config.py copy (use name config_my.py which is already in .gitignore).
DATABASE_URL:
postgresql+psycopg2://{login}:{password}@localhost/{db_name}
The list may not be 100% comprehensive, so read carefully error log.

Beware that environment variables can differ per configuration
Открыть "файлы"->"другие места"->"компьютер"->"etc"->"environment" (открыть через терминал и набрать"sudo nano environment" и добавить 3 переменные через ентр "FLASK_APP="manage.py", APP_SETTINGS="config.DevelopmentConfig", DATABASE_URL="postgresql+psycopg2://postgres:0000@localhost:5432/postgres") и потом нажать ctrl+o, enter, ctrl+x

3. Copy the https link and download the project files:
git clone https://gitlab.com/Kaidash-Dima/projecttest.git

And don't you forget that project specific commands should be run inside project root dir (where manage.py resides) if not said otherwise.

4. Create a virtual environment:
Move to some neat directory (not necessarily project root one, so that venv can be reused later thought not a best practice) and run next command:
(virtualenv -p python3 env)

python3 -m venv env

Activate environment by command:
source env/bin/activate

5. Set the required dependencies with the command:
for development:
pip install -r requirements/base.txt

6. Apply database migration script
Run the command:
flask db upgrade
If environment variables set correctly and you have clear database you will get no errors.

7. Add the Test configuration
from project root run next console command:
coverage run --source=app,manage,config -m unittest discover && coverage report -m

